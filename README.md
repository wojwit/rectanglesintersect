The repository contains a C++ project created in the Visual Studio 2015, however it is easly portable to other compilers. This is a console application which accepts a file path containing rectangles
definitions, as a parameter. It outputs a report of all intersections.
No third-party dependencies have been used.

Note: Intersections by vertex or edge are not included in the report. A product is a point or a line in such cases, so width and/or height is 0. However it is trivial to include these type of intersections, 
if necessary.


1. Build

The project source files are in the Project subfolder.

<path_to_devenv>\devenv.exe RectangleIntersect.vcxproj /build Release /projectconfig Release

<path_to_devenv> is usually "c:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\IDE"


2. Usage

RectangleIntersect.exe <path_to_a_rectangle_definition_file>

The Bin folder contains an executable compiled on Win32 platform.



