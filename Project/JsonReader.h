#pragma once

#include <string>
#include "Rectangles.h"

class CJsonReader
{
    std::string m_fileName;

    std::string getFileContent();
    std::unique_ptr<CRectangle> parseRectangleFromLine(std::string & line);
public:
    CJsonReader(const std::string fileName);
    ~CJsonReader();
    CRectangles LoadRectangles();
};

