#include "Rectangles.h"
#include <iostream>

CRectangles::CRectangles()
{
}


CRectangles::~CRectangles()
{
}

bool CRectangles::find(const std::string & hash)
{
    bool returnValue = false;

    for (vector<CRectangle>::iterator it = begin(); it != end(); it++)
    {
        if (hash == it->GetHash())
        {
            returnValue = true;
            break;
        }
    }
    return returnValue;
}

void CRectangles::Dump(std::ostream& stream)
{
    if (size() == 0)
    {
        stream << "       none" << std::endl;
    }
    else
    {
        for (auto currentRectangle : *this)
        {
            currentRectangle.Dump(stream);
        }
    }
}

void CRectangles::ReportIntersections()
{
    //CRectangles intersections;
    CRectangles work_intersections = *this;
    CRectangles temp_intersections;
    int level = 1;
    do
    {
        temp_intersections.clear();

        for (size_t i = 0; i < work_intersections.size(); i++)
        {
            CRectangle& currentRectangle = work_intersections.at(i);

            for (size_t j = i + 1; j < work_intersections.size(); j++)
            {
                CRectangle& rectangle = work_intersections.at(j);
                std::unique_ptr<CRectangle> intersect = currentRectangle.GetIntersection(rectangle);
                if (intersect)
                {
                    if ((intersect->NumOfIntersections() == (level + 1)) && !temp_intersections.find(intersect->GetHash()))
                    {
                        intersect->Dump(std::cout);
                        temp_intersections.push_back(*intersect.get());
                    }
                }
            }

        }
        if (temp_intersections.size() > 0)
        {
            level++;
            work_intersections = temp_intersections;
            //intersections.insert(intersections.end(), temp_intersections.begin(), temp_intersections.end());
        }
    } while (temp_intersections.size() > 0);

    //return intersections;
}