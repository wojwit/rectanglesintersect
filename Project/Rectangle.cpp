#include "Rectangle.h"
#include <algorithm>
#include <string>

int CRectangle::m_unique_id = 0;

int CRectangle::getUniqueId()
{
    return ++m_unique_id;
}

CRectangle::CRectangle(int x, int y, int width, int height) : 
    m_id(getUniqueId()), m_x(x), m_y(y), m_width(width), m_height(height)
{
}

CRectangle::~CRectangle()
{
}

bool CRectangle::Intersects(CRectangle &otherRectangle)
{
    bool xOverlap = 
        valueInRange(GetX(), otherRectangle.GetX(), otherRectangle.GetX() + otherRectangle.GetWidth()) ||
        valueInRange(otherRectangle.GetX(), GetX(), GetX() + GetWidth());

    bool yOverlap = 
        valueInRange(GetY(), otherRectangle.GetY(), otherRectangle.GetY() + otherRectangle.GetHeight()) ||
        valueInRange(otherRectangle.GetY(), GetY(), GetY() + GetHeight());

    return xOverlap && yOverlap;
}

void CRectangle::ReHash()
{
    std::sort(m_intersectIds.begin(), m_intersectIds.end());
    m_hash = "";
    for (std::vector<int>::iterator it = m_intersectIds.begin(); it != m_intersectIds.end(); it++)
    {
        m_hash += std::to_string(*it);
    }

}

std::unique_ptr<CRectangle> CRectangle::GetIntersection(CRectangle& otherRectangle)
{
    std::unique_ptr<CRectangle> returnIntersection;

    if (GetId() != otherRectangle.GetId())
    {
        if (Intersects(otherRectangle))
        {
            int left = std::max(GetX(), otherRectangle.GetX());
            int top = std::max(GetY(), otherRectangle.GetY());
            int right = std::min(GetX() + GetWidth(), otherRectangle.GetX() + otherRectangle.GetWidth());
            int bottom = std::min(GetY() + GetHeight(), otherRectangle.GetY() + otherRectangle.GetHeight());

            CRectangle intersect(left, top, right - left, bottom - top);
            
            bool bAdded = false;
            if (m_intersectIds.empty())
            {
                bAdded = intersect.AddInteresctId(GetId()) || bAdded;
                bAdded = intersect.AddInteresctId(otherRectangle.GetId()) || bAdded;
            }
            else
            {
                std::vector<int> intersectIds = m_intersectIds;
                intersectIds.insert(intersectIds.end(), otherRectangle.m_intersectIds.begin(), otherRectangle.m_intersectIds.end());
                
                for (auto id : intersectIds)
                {
                    bAdded = intersect.AddInteresctId(id) || bAdded;
                }
            }

            if (bAdded)
            {
                intersect.ReHash();
            }


            returnIntersection = std::make_unique<CRectangle>(intersect);
        }
    }
    return returnIntersection;
}

void CRectangle::Dump(std::ostream& stream)
{
    if (m_intersectIds.empty())
    {
        stream << "      " << m_id << ": Rectangle at (" << m_x << "," << m_y << "), w=" << m_width << ", h=" << m_height << "." << std::endl;
    }
    else
    {
        stream << "      Between rectangle "; //<< m_id;
        for (size_t i = 0; i < m_intersectIds.size() - 1; i++)
        {
            stream << m_intersectIds[i] << ((i == (m_intersectIds.size() - 2)) ? "" : ", ");
        }
        stream << " and " << m_intersectIds[m_intersectIds.size() - 1] << " at (" << m_x << "," << m_y << "), w=" << m_width << ", h=" << m_height << "." << std::endl;
    }
}

bool CRectangle::AddInteresctId(int id)
{
    bool added = false;
    if (std::find(m_intersectIds.begin(), m_intersectIds.end(), id) == m_intersectIds.end())
    {
        m_intersectIds.push_back(id);
        added = true;
    }
    return added;
}

std::string CRectangle::GetHash()
{
    return m_hash;
}