// RectangleIntersect.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include <fstream>
#include <vector>
#include "Rectangle.h"
#include "Rectangles.h"
#include "JsonReader.h"
#include <conio.h>

int main(int argc, char** argv)
{
    std::cout << "Rectangle Intersection" << std::endl;
    if (argc != 2)
    {
        std::cerr << "Usage: this_executable file_to_read" << std::endl;
        return EXIT_FAILURE;
    }

    std::string fileName = argv[1];
    std::cout << "File Name: " << fileName.c_str() << std::endl;

    CJsonReader reader(fileName);

    CRectangles rectangles = reader.LoadRectangles();
    if (rectangles.size() > 0)
    {
        std::cout << "Input:" << std::endl;

        rectangles.Dump(std::cout);

        std::cout << std::endl << "Intersections:" << std::endl;

        rectangles.ReportIntersections();
    }

    std::cout << std::endl << "Press a key ..." << std::endl;
    _getch();

    return 0;
}

