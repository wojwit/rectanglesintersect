#pragma once

#include <vector>
#include "Rectangle.h"

class CRectangles :
    public std::vector<CRectangle>
{
    bool find(const std::string & hash);
public:
    CRectangles();
    ~CRectangles();
    
    void Dump(std::ostream& stream);

    void ReportIntersections();
};

