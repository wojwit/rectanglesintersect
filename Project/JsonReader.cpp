#include "JsonReader.h"
#include <iostream>
#include <fstream>
#include <regex>


CJsonReader::CJsonReader(const std::string fileName)
{
    m_fileName = fileName;
}


CJsonReader::~CJsonReader()
{
}

std::string CJsonReader::getFileContent()
{
    std::ifstream file(m_fileName.c_str(), std::ios::in | std::ios::binary | std::ios::ate);

    if (file.is_open())
    {
        std::streampos size = file.tellg();
        char* block = new char[size];
        file.seekg(0, std::ios::beg);
        file.read(block, size);
        file.close();
        return std::string(block);
    }
    else
    {
        std::cout << "Error opening file " << m_fileName << std::endl;
        return std::string("");
    }
}

CRectangles CJsonReader::LoadRectangles()
{
    CRectangles rectangles;

    std::string content = getFileContent();

    size_t position;

    if ((position = content.find("\"rects\"")) != std::string::npos)
    {
        content = content.substr(position+7);
        if (position = content.find(":") != std::string::npos)
        {
            content = content.substr(position + 1);
            if (position = content.find("[") != std::string::npos)
            {
                content = content.substr(position + 1);

                bool something_to_parse = true;
                int counter = 0;
                while (something_to_parse)
                {
                    size_t open_bracket = content.find("{");
                    size_t close_bracket = content.find("}");
                    something_to_parse = (open_bracket != std::string::npos && close_bracket != std::string::npos);
                    if (something_to_parse && counter < 1000)
                    {
                        std::string line = content.substr(open_bracket + 1, close_bracket - open_bracket - 1);
                        std::unique_ptr<CRectangle> rectangle = parseRectangleFromLine(line);
                        if (rectangle)
                        {
                            rectangles.push_back(*rectangle.get());
                            counter++;
                        }
                        content = content.substr(close_bracket+1);
                    }
                }
            }
            else
            {
                std::cout << "Error in the file content" << std::endl;
            }
        }
        else
        {
            std::cout << "Error in the file content" << std::endl;
        }
    }
    else
    {
        std::cout << "No rectangles found" << std::endl;
    }

    return rectangles;
}

std::unique_ptr<CRectangle> CJsonReader::parseRectangleFromLine(std::string & line)
{
    std::unique_ptr<CRectangle> returnRectangle;

    std::regex rx(R"(\s*\"x\"\s*:\s*(\d+)\s*,\s*\"y\"\s*:\s*(\d+)\s*,\s*\"w\"\s*:\s*(\d+)\s*,\s*\"h\"\s*:\s*(\d+)\s*)");
    std::smatch sm;
    if (regex_search(line, sm, rx))
    {
        if (sm.size() == 5)
        {
            CRectangle rect(std::stoi(sm[1]), std::stoi(sm[2]), std::stoi(sm[3]), std::stoi(sm[4]));
            returnRectangle = std::make_unique<CRectangle>(rect);
        }
    }

    return returnRectangle;
}