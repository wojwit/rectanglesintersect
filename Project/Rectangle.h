#pragma once

#include <vector>
#include <memory>
#include <string>

class CRectangle
{
private:
    static int m_unique_id;
    static int getUniqueId();
    int m_id;
    int m_x;
    int m_y;
    int m_width;
    int m_height;
    std::string m_hash;
    bool valueInRange(int value, int min, int max)
    {
        return (value > min) && (value < max);
    }

    std::vector<int> m_intersectIds;
public:
    int NumOfIntersections() { return m_intersectIds.size(); }
    int GetX() { return m_x; }
    int GetY() { return m_y; }
    int GetWidth() { return m_width; }
    int GetHeight() { return m_height; }
    std::string GetHash();
    void ReHash();
    bool AddInteresctId(int id);
    void Dump(std::ostream& stream);
    CRectangle(int x, int y, int width, int height);
    ~CRectangle();
    int GetId() const { return m_id; }
    bool Intersects(CRectangle &otherRectangle);
    std::unique_ptr<CRectangle> GetIntersection(CRectangle& otherRectangle);
};

