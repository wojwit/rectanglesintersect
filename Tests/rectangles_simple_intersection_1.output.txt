Rectangle Intersection
File Name: c:\rectangles_simple_intersection_1.json
Input:
      1: Rectangle at (80,120), w=60, h=20.
      2: Rectangle at (100,100), w=20, h=60.

Intersections:
      Between rectangle 1 and 2 at (100,120), w=20, h=20.

Press a key ...
